/* Ananth Penghat
   April 14, 2014
   Game.java
   This program will create a space invaders type game
   based on the Immune System.
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.Timer;
import java.util.*;
import java.io.*;
import javax.imageio.*;
import java.awt.image.*;

public class Game
{
    JFrame frame;               // Holds everything in game.
    CardLayout cards;           // CardLayout for main panels.
    CardLayout switchPanel;     // CardLayout for welcoming user to game

    TitlePanel titlePanel;   // Contains main menu and instructions panels.
    Timer createGerms;       // Places germs on game board.
    Timer moveGerms;         // Moves germs down on board.
    Timer generalAnimations; // Animates user, enemies, bullets, and gameboard.
    Timer explosionTimer;    // Creates explosion animation.

    boolean pauseBoolean = false; // Determines whether to listen to keys or not when paused.
    boolean returnPlay = false;   // Determines when to move enemies back on game board.
    int questionPicker;          // Determines which question to ask user.
    int mistakesMade;       // Number of errors user had made.
    Font font, mediumFont, smallFont; // Fonts that will be used throughout game.

    // Switch between general info and enemy info panels.
    JRadioButton infoPageButton, enemyPageButton;
    // Prevents user from choosing two JRadioButtons at once.
    ButtonGroup changePageButtons;
    // Switch between panels containing info on the Immune System.
    JComboBox changePageBox, returnPageBox;

    // Strings/Files relating to the setup of the game.
    File instructionsFile;	    // File containing instructions '.txt' file.
    Scanner input;			    // Scanner that reads instructions 'txt' file.
    String introString;         // Contains general overiview of game.
    String cellIntroString;     // Contains info on user's sprite.
    String controlString;	    // Contains info on how to do basic actions in game.
    String questionIntroString; // Contains intro to enemies.
    String enemyPanelString;    // Contains info on panel with info about enemies.
    String triviaString;        // Contains info on panel that has questions.
    String purpleGermString, orangeGermString, blackGermString;  // Informs user about enemies.
    String gameOverString;      // Contains info about losing the game and replaying.

    // Strings relating to the Immune System.
    String generalImmuneString; // Contains general info about Immune System.
    String detailImmuneString;  // Contains in depth info about Immune System.
    String wbcGeneralString;    // Contains general info about white blood cells.
    String leukInfoString;      // Contains info about leukocytes.
    String wbcDetailString;     // Contains detailed info about white blood cells.
    String antibioticsString;   // Contains info about antibiotics.

    // Strings/Files which output user's score onto a new '.txt' file.
    File scoreOutputFile;    // Contains '.txt' file with user's score.
    PrintWriter scoreOutput; // Writes the user's score.

    // Strings/Files that contain questions/answers for game.
    File questionsFile;	    // File containing '.txt' file with questions.
    Scanner questionInput;	// Scanner that reads question .txt file.
    File answersFile;       // File containing '.txt' file with answers.
    Scanner answerInput;    // Scanner that reads answers '.txt' file.

    String questionString;	          // Contains the questions.
    ArrayList<String> questionHolder; // Holds all question Strings.
    String answerString;              // Contains the answers.
    ArrayList<String> answerHolder;   // Holds all the answer Strings.

    /* ArrayLists that contain BufferedImages for user/enemies/explosions. */
    // Contains BufferedImages of white blood cell [user].
    ArrayList<BufferedImage> objectCells;
    // Contains BufferedImages of bullets.
    ArrayList<BufferedImage> objectBullets;
	// Contains BufferedImages of explosions.
    ArrayList<BufferedImage> objectExplosion;
    // Contains ArrayLists that contains BufferedImages of enemies.
    ArrayList<ArrayList<BufferedImage>> germAnimations;

    // Images for blood cell sprite sheets and instances of cell
    BufferedImage cellSprite, drawing;
    // Images for germ sprite sheets and different colored germs.
    BufferedImage germSprite, orangeGerm, blackGerm, purpleGerm;
    // Images of explosion sprite sheet and instances of cell.
    BufferedImage explosionSprite, kaboom, explosionDrawing;
    // Images of bullet sprite sheet and instances of bullet.
    BufferedImage bulletSprite, bulletDrawing;
    // Image for background.
    BufferedImage backgroundSprite;

    public Game()
    {
        // Read in text that teaches user how to play.
        instructionsFile = new File("instructions.txt");
        // Read in text which contains questions that will be asked.
        questionsFile = new File("questions.txt");
        // Reads in the answers to the questions.
        answersFile = new File("answers.txt");

        try // Try to read '.txt' files.
        {
            input = new Scanner(instructionsFile);
            questionInput = new Scanner(questionsFile);
            answerInput = new Scanner(answersFile);
        }
        catch (FileNotFoundException fnfe) // Print error message if unable to read '.txt' file.
        {
            System.err.println("ERROR: Cannot open text file");
            System.exit(1);  // Close game.
        }

        // Store general info/gameplay instructions text into strings.
        introString = input.nextLine();
        cellIntroString = input.nextLine();
        controlString = input.nextLine();
        questionIntroString = input.nextLine();
        enemyPanelString = input.nextLine();
        triviaString = input.nextLine();
        purpleGermString = input.nextLine();
        orangeGermString = input.nextLine();
        blackGermString = input.nextLine();
        // Store info about Immune System into strings.
        generalImmuneString = input.nextLine();
        detailImmuneString = input.nextLine();
        wbcGeneralString = input.nextLine();
        leukInfoString = input.nextLine();
        wbcDetailString = input.nextLine();
        antibioticsString = input.nextLine();
        gameOverString = input.nextLine();
        input.close();	// Stop reading from '.txt' file.

		// Store questions/answers text into Strings and then an ArrayList.
        questionHolder = new ArrayList<String>();
        answerHolder = new ArrayList<String>();
        for (int ii = 0; ii < 18; ii++)  // Run 18 times [number of questions].
        {
            questionString = questionInput.nextLine();
            questionHolder.add(questionString);
            answerString = answerInput.nextLine();
            answerHolder.add(answerString);
        }
        questionInput.close();
        answerInput.close();

        // ArrayLists that contain the sprites.
        objectCells = new ArrayList<BufferedImage>();
        objectBullets = new ArrayList<BufferedImage>();
        objectExplosion = new ArrayList<BufferedImage>();
        germAnimations = new ArrayList<ArrayList<BufferedImage>>();

        try  // Try to read in sprite sheets.
        {
            cellSprite = ImageIO.read(new File("wbloodcells.png"));
            backgroundSprite = ImageIO.read(new File("redpixel.jpg"));
            germSprite = ImageIO.read(new File("germs.gif"));
            explosionSprite = ImageIO.read(new File("explosion.png"));
            bulletSprite = ImageIO.read(new File("bullets.png"));
        }
        catch (IOException ioe) // Print error message if error
        {
            System.out.println("Image error.");
        }

        for (int ii = 0; ii <  4; ii++)  // Get subimages of white blood cell sprite sheet.
        {
            BufferedImage image = cellSprite.getSubimage(ii * 90, 0, 94, 93);
            objectCells.add(image);
        }
		// Add three ArrayList<BufferedImage>() to germAnimations to hold each color of enemy.
		for (int ii = 0; ii < 3; ii++)
		{
			germAnimations.add(new ArrayList<BufferedImage>());
		}

        for (int ii = 80; ii <= 200; ii += 40) // Get subimages of white blood cells [user] from sprite sheet.
        {
        	// Add orange colored BufferedImages to first ArrayList in germAnimations.
            BufferedImage orangeImage = germSprite.getSubimage(ii, 60, 39, 60);
	    	germAnimations.get(0).add(orangeImage);

            // Add purple colored BufferedImages to first ArrayList in germAnimations.
            BufferedImage purpleImage = germSprite.getSubimage(ii, 240, 39, 60);
	    	germAnimations.get(1).add(purpleImage);

            // Add black colored BufferedImages to first ArrayList in germAnimations.
            BufferedImage blackImage = germSprite.getSubimage(ii, 180, 39, 60);
	    	germAnimations.get(2).add(blackImage);
        }

        for (int ii = 0; ii <= 69; ii += 23)   // Get subimages of bullets from sprite sheet.
        {
            BufferedImage bulletImage = bulletSprite.getSubimage(ii, 0, 23, 25);
            objectBullets.add(bulletImage);
        }

        int kaboomWidth = 39;   // Height of each subimage of explosion.
        int kaboomHeight = 39;  // Width of each subimage of explosion.
        for (int ii = 0; ii < 13; ii++) // Get subimages of explosions from sprite sheet.
        {
            kaboom = explosionSprite.getSubimage(ii * kaboomWidth,
					      						157 - kaboomHeight,
					      						kaboomWidth, kaboomHeight);
            									objectExplosion.add(kaboom);
        }
    }

    public static void main (String [] args)
    {
        Game g = new Game();
        g.run();
    }

    public void run()
    {
        // Initialize JFrame.
        frame = new JFrame("Final - ANANTOMY ARCADE!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(800, 600);    // 800x600 pixels.
        frame.setLocation(40, 40);  // JFrame appears here on screen.

        titlePanel = new TitlePanel();  // Initialize panel for main menu.
        frame.setLayout(new BorderLayout()); // Set to BorderLayout.

        // Add JPanel to frame.
        frame.getContentPane().add(titlePanel, BorderLayout.CENTER);

        frame.setResizable(false);  // JFrame set at 800x600.
        frame.setVisible(true);     // JFrame is visible on screen.
        frame.setFocusable(true);   // JFrame gets focus.
    }

    // Main Panel. Holds the other JPanels of this game.
    class TitlePanel extends JPanel
    {
		// Main screens that give info to user.
        CheckerBoard menuPanel, infoPanel, immunePanel, immunePanel2, enemyInfoPanel;
        QuestionPanel triviaPanel; // Displays the question to the user.
        WBCGamePanel wbc;	// Game panel.
		// Panels that tell user they're right.
	    CorrectAnswer cp1, cp2, cp3, cp4, cp5, cp6, cp7, cp8, cp9,
	    			  cp10, cp11, cp12, cp13, cp14, cp15, cp16, cp17, cp18;
        // Panels that tell user they're wrong.
	    IncorrectAnswer ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9,
	    				ip10, ip11, ip12, ip13, ip14, ip15, ip16, ip17, ip18;
	    // Panels that contain the questions.
    	Question q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12,
    			 q13, q14, q15, q16, q17, q18;
    	GameOver gameOverPanel;

        public TitlePanel()
        {
            // Initialize CardLayout and set titleMenu to CardLayout.
            switchPanel = new CardLayout();
            this.setLayout(switchPanel);

            // Initialize game related JPanels and set Layout to 'null'.
            menuPanel = new CheckerBoard();
            menuPanel.setLayout(null);
            infoPanel = new CheckerBoard();
            infoPanel.setLayout(null);
            enemyInfoPanel = new CheckerBoard();
            enemyInfoPanel.setLayout(null);
            immunePanel = new CheckerBoard();
            immunePanel2 = new CheckerBoard();
            immunePanel2.setLayout(null);
            immunePanel.setLayout(null);
            triviaPanel = new QuestionPanel();
            triviaPanel.setLayout(null);
            wbc = new WBCGamePanel();
            wbc.setLayout(null);
            gameOverPanel = new GameOver();
            gameOverPanel.setLayout(null);

			// Initialize question panels so they contain information.
            // First Question.
            q1 = new Question(0,
			                  "To keep the body healthy and safe from viruses and germs.",
			                  "To digest food and absord nutrients.",
			                  "To do your taxes.");
            q1.setLayout(null);
            // Second Question.
            q2 = new Question(1,
                              "Five.",
                              "Three.",
                              "2\u03C0r.");
            q2.setLayout(null);
            // Third Question.
            q3 = new Question(2,
                              "White blood cells attack germs, viruses, and bacteria.",
                              "White blood cells pump blood and oxygen around the body.",
                              "White blood cells fight Communists.");
            q3.setLayout(null);
            // Fourth Question.
            q4 = new Question(3,
                              "Special cells produce chemicals which can detect them.",
                              "White blood cells check every other cell for viruses/germs.",
                              "The body googles the location of each virus/germ.");
            q4.setLayout(null);
            // Fifth Question.
            q5 =  new Question(4,
                                "Human Immunodeficiency Virus.",
                                "Human Immunocisitic Virus.",
                                "Acquired Immunodeficiency Syndrome");
            q5.setLayout(null);
            // Sixth Question.
            q6 = new Question(5,
                              "A substance that causes the immune system to produce antibodies.",
                              "A type of chemical that detects foreign substances.",
                              "A type of ice-cream.");
            q6.setLayout(null);
            // Seventh Question.
            q7 = new Question(6,
                              "Leukocytes.",
                              "Lymph cells.",
                              "The French.");
            q7.setLayout(null);
            // Eight Question.
            q8 = new Question(7,
                              "Spleen.",
                              "Gallbladder.",
                              "Rectum.");
            q8.setLayout(null);
            // Ninth Question.
            q9 = new Question(8,
                              "Viruses, bacteria, fungi, and parasites.",
                              "Viruses, bacteria, ticks, and parasites.",
                              "Lennon, McCartney, Harrison, and Starr.");
            q9.setLayout(null);
            // Tenth Question.
            q10 = new Question(9,
                               "Immunizations.",
                               "Band-aids.",
                               "Cigars.");
            q10.setLayout(null);
            // Eleventh Question.
            q11 = new Question(10,
                               "You're protected.",
                               "You're in danger.",
                               "You need to lose some weight.");
            q11.setLayout(null);
            // Twelvth Question.
            q12 = new Question(11,
            				   "Allergy.",
            				   "Immune Overload.",
            				   "Emotional Breakdown.");
            q12.setLayout(null);
            // Thirteenth Question.
            q13 = new Question(12,
            			       "Watching all 9 seasons of Seinfield.",
            			       "Having a healthy diet.",
            			       "Working out your body daily.");
            q13.setLayout(null);
            // Fourteenth Question.
            q14 = new Question(13,
            				   "Thymus Gland.",
            				   "Spleen.",
            				   "Somalia.");
            q14.setLayout(null);
            // Fifteenth Question.
            q15 = new Question(14,
            				   "When the Immune System cannot fight disease.",
            				   "When the Immune System attacks friendly cells.",
            				   "When the Immune System forgets to pay it's gas bill.");
            q15.setLayout(null);
            // Sixteenth Question.
            q16 = new Question(15,
            				   "AIDS.",
            				   "Leukemia.",
            				   "Explosive Diarrhea.");
            q16.setLayout(null);
            // Seventeenth Question.
            q17 = new Question(16,
            				   "When the Immune System attacks friendly cells.",
            				   "When the Immune System cannot fight disease.",
            				   "When the Immune System gets rejected by a girl.");
            q17.setLayout(null);
            // Eighteenth Question.
            q18 = new Question(17,
            				   "Lupus.",
            				   "Broken Finger.",
            				   "Erectile Disfuntion.");
            q18.setLayout(null);

			// Initialize panels that tell user they're right.
            cp1 = new CorrectAnswer();
            cp1.setLayout(null);
            cp2 = new CorrectAnswer();
            cp2.setLayout(null);
            cp3 = new CorrectAnswer();
            cp3.setLayout(null);
            cp4 = new CorrectAnswer();
            cp4.setLayout(null);
            cp5 = new CorrectAnswer();
            cp5.setLayout(null);
            cp6 = new CorrectAnswer();
            cp6.setLayout(null);
            cp7 = new CorrectAnswer();
            cp7.setLayout(null);
            cp8 = new CorrectAnswer();
            cp8.setLayout(null);
            cp9 = new CorrectAnswer();
            cp9.setLayout(null);
            cp10 = new CorrectAnswer();
            cp10.setLayout(null);
            cp11 = new CorrectAnswer();
            cp11.setLayout(null);
            cp12 = new CorrectAnswer();
            cp12.setLayout(null);
            cp13 = new CorrectAnswer();
            cp13.setLayout(null);
            cp14 = new CorrectAnswer();
            cp14.setLayout(null);
            cp15 = new CorrectAnswer();
            cp15.setLayout(null);
            cp16 = new CorrectAnswer();
            cp16.setLayout(null);
            cp17 = new CorrectAnswer();
            cp17.setLayout(null);
            cp18 = new CorrectAnswer();
            cp18.setLayout(null);

			// Initialize panels that tell user they're wrong.
            ip1 = new IncorrectAnswer();
            ip1.setLayout(null);
            ip2 = new IncorrectAnswer();
            ip2.setLayout(null);
            ip3 = new IncorrectAnswer();
            ip3.setLayout(null);
            ip4 = new IncorrectAnswer();
            ip4.setLayout(null);
            ip5 = new IncorrectAnswer();
            ip5.setLayout(null);
            ip6 = new IncorrectAnswer();
            ip6.setLayout(null);
            ip7 = new IncorrectAnswer();
            ip7.setLayout(null);
            ip8 = new IncorrectAnswer();
            ip8.setLayout(null);
            ip9 = new IncorrectAnswer();
            ip9.setLayout(null);
            ip10 = new IncorrectAnswer();
            ip10.setLayout(null);
            ip11 = new IncorrectAnswer();
            ip11.setLayout(null);
            ip12 = new IncorrectAnswer();
            ip12.setLayout(null);
            ip13 = new IncorrectAnswer();
            ip13.setLayout(null);
            ip14 = new IncorrectAnswer();
            ip14.setLayout(null);
            ip15 = new IncorrectAnswer();
            ip15.setLayout(null);
            ip16 = new IncorrectAnswer();
            ip16.setLayout(null);
            ip17 = new IncorrectAnswer();
            ip17.setLayout(null);
            ip18 = new IncorrectAnswer();
            ip18.setLayout(null);

            // Add JPanels to CardLayout 'switchPanel'.
            this.add(menuPanel, "Main Menu");
            this.add(infoPanel, "Instructions");
            this.add(enemyInfoPanel, "Info about Germs");
            this.add(immunePanel, "Immune System");
            this.add(immunePanel2, "Immune System Page 2");
            this.add(triviaPanel, "Ask Questions");
            this.add(wbc, "WBC Mini-Game");
            this.add(gameOverPanel, "Game Over");

            // Add JPanels that are shown if answer was right.
            this.add(cp1, "Correct Answer 1");
	        this.add(cp2, "Correct Answer 2");
	        this.add(cp3, "Correct Answer 3");
	        this.add(cp4, "Correct Answer 4");
	        this.add(cp5, "Correct Answer 5");
	        this.add(cp6, "Correct Answer 6");
	        this.add(cp7, "Correct Answer 7");
	        this.add(cp8, "Correct Answer 8");
	        this.add(cp9, "Correct Answer 9");
	        this.add(cp10, "Correct Answer 10");
	        this.add(cp11, "Correct Answer 11");
	        this.add(cp12, "Correct Answer 12");
	        this.add(cp13, "Correct Answer 13");
	        this.add(cp14, "Correct Answer 14");
	        this.add(cp15, "Correct Answer 15");
	        this.add(cp16, "Correct Answer 16");
            this.add(cp17, "Correct Answer 17");
            this.add(cp18, "Correct Answer 18");

            // Add JPanels that are shown if answer wrong.
            this.add(ip1, "Incorrect Answer 1");
	        this.add(ip2, "Incorrect Answer 2");
	        this.add(ip3, "Incorrect Answer 3");
	        this.add(ip4, "Incorrect Answer 4");
	        this.add(ip5, "Incorrect Answer 5");
	        this.add(ip6, "Incorrect Answer 6");
	        this.add(ip7, "Incorrect Answer 7");
	        this.add(ip8, "Incorrect Answer 8");
	        this.add(ip9, "Incorrect Answer 9");
	        this.add(ip10, "Incorrect Answer 10");
	        this.add(ip11, "Incorrect Answer 11");
	        this.add(ip12, "Incorrect Answer 12");
	        this.add(ip13, "Incorrect Answer 13");
	        this.add(ip14, "Incorrect Answer 14");
	        this.add(ip15, "Incorrect Answer 15");
	        this.add(ip16, "Incorrect Answer 16");
            this.add(ip17, "Incorrect Answer 17");
            this.add(ip18, "Incorrect Answer 18");

            // Add Question JPanels with questions to CardLayout.
            this.add(q1, "First Question");
            this.add(q2, "Second Question");
            this.add(q3, "Third Question");
            this.add(q4, "Fourth Question");
            this.add(q5, "Fifth Question");
            this.add(q6, "Sixth Question");
            this.add(q7, "Seventh Question");
            this.add(q8, "Eigth Question");
            this.add(q9, "Ninth Question");
            this.add(q10, "Tenth Question");
            this.add(q11, "Eleventh Question");
            this.add(q12, "Twelvth Question");
            this.add(q13, "Thirteenth Question");
            this.add(q14, "Fourteenth Question");
            this.add(q15, "Fifteenth Question");
            this.add(q16, "Sixteenth Question");
            this.add(q17, "Seventeenth Question");
            this.add(q18, "Eighteenth Question");

	        // Various sized fonts for different situations.
            font = new Font("Arial", Font.BOLD, 20);
            mediumFont = new Font("Arial", Font.BOLD, 18);
            smallFont = new Font("Arial", Font.BOLD, 16);

            // Game title JLabels.
            JLabel anatomyLabel = new JLabel("ANATOMY");
            JLabel arcadeLabel = new JLabel("ARCADE");
            anatomyLabel.setFont(new Font("Arial", Font.BOLD, 50));
            arcadeLabel.setFont(new Font("Arial", Font.BOLD, 50));
            anatomyLabel.setForeground(Color.white);
            arcadeLabel.setForeground(Color.white);
            anatomyLabel.setBounds(275, 35, 270, 50);
            arcadeLabel.setBounds(295, 60, 220, 105);
            menuPanel.add(anatomyLabel);
            menuPanel.add(arcadeLabel);

            // Goes to panel where user plays the game.
            JButton playButton = new JButton("Play!");
            playButton.setFont(font);
            playButton.addActionListener(new ButtonPlayListener());
            playButton.setBounds(305, 180, 188, 60);
            menuPanel.add(playButton);

            // Goes to panel on how to play.
            JButton infoButton = new JButton("How to Play!");
            infoButton.setFont(font);
            infoButton.addActionListener(new ButtonInfoListener());
            infoButton.setBounds(305, 260, 188, 60);
            menuPanel.add(infoButton);

            // Returns to Main Menu panel from info panel.
            JButton infoReturnButton = new JButton("Return to Main Menu");
            infoReturnButton.setFont(font);
            infoReturnButton.addActionListener(new ButtonMainListener());
            infoReturnButton.setBounds(0, 0, 800, 30);
            infoPanel.add(infoReturnButton);

            // Returns to Main Menu panel from enemy info panel.
            JButton enemyReturnButton = new JButton("Return to Main Menu");
            enemyReturnButton.setFont(font);
            enemyReturnButton.addActionListener(new ButtonMainListener());
            enemyReturnButton.setBounds(0, 0, 800, 30);
            enemyInfoPanel.add(enemyReturnButton);

	        // Tells user the basics of the game.
            JTextArea helpTextArea = new JTextArea(introString);
            helpTextArea.setForeground(Color.white);
            helpTextArea.setFont(mediumFont);
            helpTextArea.setEditable(false);
            helpTextArea.setLineWrap(true);
            helpTextArea.setWrapStyleWord(true);
            helpTextArea.setOpaque(false);
            helpTextArea.setBounds(10, 35, 780, 125);
            infoPanel.add(helpTextArea);

            // Inform user about player (white blood cell).
            JTextField cellIntroField = new JTextField(cellIntroString);
            cellIntroField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
            cellIntroField.setForeground(Color.WHITE);
            cellIntroField.setBackground(new Color(0, 0, 0, 0));
            cellIntroField.setFont(mediumFont);
            cellIntroField.setEditable(false);
            cellIntroField.setBounds(10, 196, 225, 15);
            infoPanel.add(cellIntroField);
            // Icons of player (white blood cell).
            ImageIcon cellIcon = new ImageIcon(objectCells.get(0));
            ImageIcon cellIcon2 = new ImageIcon(objectCells.get(1));
            ImageIcon cellIcon3 = new ImageIcon(objectCells.get(2));
            ImageIcon cellIcon4 = new ImageIcon(objectCells.get(3));
            // Store icons on JLabel
            JLabel cellPicLabel = new JLabel(cellIcon);
            JLabel cellPicLabel2 = new JLabel(cellIcon2);
            JLabel cellPicLabel3 = new JLabel(cellIcon3);
            JLabel cellPicLabel4 = new JLabel(cellIcon4);
            // Set location of Icon-filled JLabels.
            cellPicLabel.setBounds(278, 175, 70, 90);
            cellPicLabel2.setBounds(388, 175, 70, 90);
            cellPicLabel3.setBounds(498, 180, 90, 85);
            cellPicLabel4.setBounds(633, 175, 90, 90);
            // Add Icon-filled JLabels to panel.
            infoPanel.add(cellPicLabel);
            infoPanel.add(cellPicLabel2);
            infoPanel.add(cellPicLabel3);
            infoPanel.add(cellPicLabel4);

            // Informs user on how to control/play game.
            JTextArea controlArea = new JTextArea(controlString);
            controlArea.setForeground(Color.white);
            controlArea.setFont(mediumFont);
            controlArea.setEditable(false);
            controlArea.setLineWrap(true);
            controlArea.setWrapStyleWord(true);
            controlArea.setOpaque(false);
            controlArea.setBounds(10, 265, 780, 125);
            infoPanel.add(controlArea);

            // Tells user about in depth rules and how to end game.
            JTextArea questionRulesArea = new JTextArea(questionIntroString);
            questionRulesArea.setForeground(Color.white);
            questionRulesArea.setFont(mediumFont);
            questionRulesArea.setEditable(false);
            questionRulesArea.setLineWrap(true);
            questionRulesArea.setWrapStyleWord(true);
            questionRulesArea.setOpaque(false);
            questionRulesArea.setBounds(10, 375, 780, 175);
            infoPanel.add(questionRulesArea);

            // Flips to first info page when clicked.
            infoPageButton = new JRadioButton();
            infoPageButton.addActionListener(new RadioListener());
            infoPageButton.setOpaque(false);
            infoPageButton.setForeground(Color.white);
            infoPageButton.setFont(mediumFont);
            infoPageButton.setBounds(305, 520, 200, 40);
            infoPageButton.setText("General Info Page");
            enemyInfoPanel.add(infoPageButton);

            // Flips to page about enemies when clicked.
            enemyPageButton = new JRadioButton();
            enemyPageButton.addActionListener(new RadioListener());
            enemyPageButton.setOpaque(false);
            enemyPageButton.setForeground(Color.white);
            enemyPageButton.setFont(mediumFont);
            enemyPageButton.setBounds(305, 520, 200, 40);
            enemyPageButton.setText("Enemy Info Page");
            infoPanel.add(enemyPageButton);

            // Tells user about neccesity of learning about enemies.
            JTextArea enemyPanelArea = new JTextArea(enemyPanelString);
            enemyPanelArea.setForeground(Color.white);
            enemyPanelArea.setFont(font);
            enemyPanelArea.setEditable(false);
            enemyPanelArea.setLineWrap(true);
            enemyPanelArea.setWrapStyleWord(true);
            enemyPanelArea.setOpaque(false);
            enemyPanelArea.setBounds(10, 35, 780, 75);
            enemyInfoPanel.add(enemyPanelArea);

            // ButtonGroup so that user can only pick one JRadioButton at a time.
            changePageButtons = new ButtonGroup();
            changePageButtons.add(infoPageButton);
            changePageButtons.add(enemyPageButton);

            // Official name of purple enemy.
            JTextField purpleNameField = new JTextField("Cytomegalovirus");
            purpleNameField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
            purpleNameField.setForeground(Color.WHITE);
            purpleNameField.setBackground(new Color(0, 0, 0, 0));
            purpleNameField.setFont(mediumFont);
            purpleNameField.setEditable(false);
            purpleNameField.setBounds(10, 130, 160, 25);
            enemyInfoPanel.add(purpleNameField);
            //  Tell user about purple enemy.
            JTextArea purpleGermArea = new JTextArea(purpleGermString);
            purpleGermArea.setForeground(Color.white);
            purpleGermArea.setFont(smallFont);
            purpleGermArea.setEditable(false);
            purpleGermArea.setLineWrap(true);
            purpleGermArea.setWrapStyleWord(true);
            purpleGermArea.setOpaque(false);
            purpleGermArea.setBounds(190, 135, 590, 85);
            enemyInfoPanel.add(purpleGermArea);
            // Images of  purple enemies (germs).
            ImageIcon purpleGermIcon = new ImageIcon(germAnimations.get(1).get(0));
            ImageIcon purpleGermIcon2 = new ImageIcon(germAnimations.get(1).get(1));
            ImageIcon purpleGermIcon3 = new ImageIcon(germAnimations.get(1).get(2));
            ImageIcon purpleGermIcon4 = new ImageIcon(germAnimations.get(1).get(3));
            // Store purple icons in JLabels.
            JLabel purpleGermLabel = new JLabel(purpleGermIcon);
            JLabel purpleGermLabel2 = new JLabel(purpleGermIcon2);
            JLabel purpleGermLabel3 = new JLabel(purpleGermIcon3);
            JLabel purpleGermLabel4 = new JLabel(purpleGermIcon4);
            // Set location of Icon-filled JLabels.
            purpleGermLabel.setBounds(-15, 135, 80, 80);
            purpleGermLabel2.setBounds(25, 132, 80, 80);
            purpleGermLabel3.setBounds(73, 135, 80, 80);
            purpleGermLabel4.setBounds(110, 135, 80, 80);
            // Add Icon-filled JLabels to panel.
            enemyInfoPanel.add(purpleGermLabel);
            enemyInfoPanel.add(purpleGermLabel2);
	        enemyInfoPanel.add(purpleGermLabel3);
            enemyInfoPanel.add(purpleGermLabel4);

           // Official name of the orange enemy.
            JTextField orangeNameField = new JTextField("Coronavirus");
            orangeNameField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
            orangeNameField.setForeground(Color.WHITE);
            orangeNameField.setBackground(new Color(0, 0, 0, 0));
            orangeNameField.setFont(mediumFont);
            orangeNameField.setEditable(false);
            orangeNameField.setBounds(10, 265, 160, 25);
            enemyInfoPanel.add(orangeNameField);
            // Tell user about orange enemy.
            JTextArea orangeGermArea = new JTextArea(orangeGermString);
            orangeGermArea.setForeground(Color.white);
            orangeGermArea.setFont(smallFont);
            orangeGermArea.setEditable(false);
            orangeGermArea.setLineWrap(true);
            orangeGermArea.setWrapStyleWord(true);
            orangeGermArea.setOpaque(false);
            orangeGermArea.setBounds(190, 270, 610, 85);
            enemyInfoPanel.add(orangeGermArea);
            // Images of orange enemies (germs).
            ImageIcon orangeGermIcon = new ImageIcon(germAnimations.get(0).get(0));
            ImageIcon orangeGermIcon2 = new ImageIcon(germAnimations.get(0).get(1));
            ImageIcon orangeGermIcon3 = new ImageIcon(germAnimations.get(0).get(2));
            ImageIcon orangeGermIcon4 = new ImageIcon(germAnimations.get(0).get(3));
            // Store orange icons in JLabels.
            JLabel orangeGermLabel = new JLabel(orangeGermIcon);
            JLabel orangeGermLabel2 = new JLabel(orangeGermIcon2);
            JLabel orangeGermLabel3 = new JLabel(orangeGermIcon3);
            JLabel orangeGermLabel4 = new JLabel(orangeGermIcon4);
            // Set location of Icon-filled JLabels.
            orangeGermLabel.setBounds(-15, 280, 80, 80);
            orangeGermLabel2.setBounds(25, 278, 80, 80);
            orangeGermLabel3.setBounds(73, 280, 80, 80);
            orangeGermLabel4.setBounds(110, 282, 80, 80);
            // Add Icon-filled JLabels to panel.
            enemyInfoPanel.add(orangeGermLabel);
            enemyInfoPanel.add(orangeGermLabel2);
	        enemyInfoPanel.add(orangeGermLabel3);
            enemyInfoPanel.add(orangeGermLabel4);

            // Official name of black enemy.
            JTextField blackNameField = new JTextField("Adenovirus");
            blackNameField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
            blackNameField.setForeground(Color.WHITE);
            blackNameField.setBackground(new Color(0, 0, 0, 0));
            blackNameField.setFont(mediumFont);
            blackNameField.setEditable(false);
            blackNameField.setBounds(10, 395, 160, 25);
            enemyInfoPanel.add(blackNameField);
            // Tell user about black enemy.
            JTextArea blackGermArea = new JTextArea(blackGermString);
            blackGermArea.setForeground(Color.white);
            blackGermArea.setFont(smallFont);
            blackGermArea.setEditable(false);
            blackGermArea.setLineWrap(true);
            blackGermArea.setWrapStyleWord(true);
            blackGermArea.setOpaque(false);
            blackGermArea.setBounds(190, 398, 580, 120);
            enemyInfoPanel.add(blackGermArea);
	        // Images of black enemies (germs).
            ImageIcon blackGermIcon = new ImageIcon(germAnimations.get(2).get(0));
            ImageIcon blackGermIcon2 = new ImageIcon(germAnimations.get(2).get(1));
            ImageIcon blackGermIcon3 = new ImageIcon(germAnimations.get(2).get(2));
            ImageIcon blackGermIcon4 = new ImageIcon(germAnimations.get(2).get(3));
            // Store black icons in JLabels.
            JLabel blackGermLabel = new JLabel(blackGermIcon);
            JLabel blackGermLabel2 = new JLabel(blackGermIcon2);
            JLabel blackGermLabel3 = new JLabel(blackGermIcon3);
            JLabel blackGermLabel4 = new JLabel(blackGermIcon4);
            // Set location of Icon-filled JLabels.
            blackGermLabel.setBounds(-15, 406, 80, 80);
            blackGermLabel2.setBounds(25, 404, 80, 80);
            blackGermLabel3.setBounds(73, 407, 80, 80);
            blackGermLabel4.setBounds(110, 407, 80, 80);
            // Add Icon-filled JLabels to panel.
            enemyInfoPanel.add(blackGermLabel);
            enemyInfoPanel.add(blackGermLabel2);
	    	enemyInfoPanel.add(blackGermLabel3);
            enemyInfoPanel.add(blackGermLabel4);

            // Goes to panel about the Immune System
            JButton immuneSystemButton = new JButton("Immune System!");
            immuneSystemButton.setFont(mediumFont);
            immuneSystemButton.addActionListener(new ButtonImmuneListener());
            immuneSystemButton.setBounds(305, 340, 188, 60);
            menuPanel.add(immuneSystemButton);

            // Returns to Main Menu panel from Immune System Panel.
            JButton immuneReturnButton = new JButton("Return to Main Menu");
            immuneReturnButton.setFont(font);
            immuneReturnButton.addActionListener(new ButtonMainListener());
            immuneReturnButton.setBounds(0, 0, 800, 30);
            immunePanel.add(immuneReturnButton);

            // Tells user the basics of the Immune System.
            JTextArea generalImmuneArea = new JTextArea(generalImmuneString);
            generalImmuneArea.setForeground(Color.white);
            generalImmuneArea.setFont(font);
            generalImmuneArea.setEditable(false);
            generalImmuneArea.setLineWrap(true);
            generalImmuneArea.setWrapStyleWord(true);
            generalImmuneArea.setOpaque(false);
            generalImmuneArea.setBounds(10, 45, 780, 100);
            immunePanel.add(generalImmuneArea);

            // Tells user more in depth info about Immune System.
            JTextArea detailImmuneArea = new JTextArea(detailImmuneString);
            detailImmuneArea.setForeground(Color.white);
            detailImmuneArea.setFont(font);
            detailImmuneArea.setEditable(false);
            detailImmuneArea.setLineWrap(true);
            detailImmuneArea.setWrapStyleWord(true);
            detailImmuneArea.setOpaque(false);
            detailImmuneArea.setBounds(10, 185, 780, 100);
            immunePanel.add(detailImmuneArea);

            // General info about white blood cells.
            JTextArea wbcGeneralArea = new JTextArea(wbcGeneralString);
            wbcGeneralArea.setForeground(Color.white);
            wbcGeneralArea.setFont(font);
            wbcGeneralArea.setEditable(false);
            wbcGeneralArea.setLineWrap(true);
            wbcGeneralArea.setWrapStyleWord(true);
            wbcGeneralArea.setOpaque(false);
            wbcGeneralArea.setBounds(10, 325, 780, 100);
            immunePanel.add(wbcGeneralArea);

            // Returns to Main Menu panel from second Immune System Panel.
            JButton immune2ReturnButton = new JButton("Return to Main Menu");
            immune2ReturnButton.setFont(font);
            immune2ReturnButton.addActionListener(new ButtonMainListener());
            immune2ReturnButton.setBounds(0, 0, 800, 30);
            immunePanel2.add(immune2ReturnButton);

            // Flips to second panel about Immune System.
            changePageBox = new JComboBox();
			changePageBox.addActionListener(new ChangeBoxListener());
			changePageBox.addItem("Switch Pages!");
            changePageBox.addItem("Page Two");
            changePageBox.setFont(font);
            changePageBox.setBounds(299, 465, 200, 50);
            immunePanel.add(changePageBox);

            // Contains info about leukocytes.
	        JTextArea leukInfoArea = new JTextArea(leukInfoString);
            leukInfoArea.setForeground(Color.white);
            leukInfoArea.setFont(font);
            leukInfoArea.setEditable(false);
            leukInfoArea.setLineWrap(true);
            leukInfoArea.setWrapStyleWord(true);
            leukInfoArea.setOpaque(false);
            leukInfoArea.setBounds(10, 45, 780, 125);
            immunePanel2.add(leukInfoArea);

            // Contains in depth info about white blood cells.
            JTextArea wbcDetailArea = new JTextArea(wbcDetailString);
            wbcDetailArea.setForeground(Color.white);
            wbcDetailArea.setFont(font);
            wbcDetailArea.setEditable(false);
            wbcDetailArea.setLineWrap(true);
            wbcDetailArea.setWrapStyleWord(true);
            wbcDetailArea.setOpaque(false);
            wbcDetailArea.setBounds(10, 175, 780, 125);
            immunePanel2.add(wbcDetailArea);

            // Contains info about antibiotics.
            JTextArea antibioticsArea = new JTextArea(antibioticsString);
            antibioticsArea.setForeground(Color.white);
            antibioticsArea.setFont(font);
            antibioticsArea.setEditable(false);
            antibioticsArea.setLineWrap(true);
            antibioticsArea.setWrapStyleWord(true);
            antibioticsArea.setOpaque(false);
            antibioticsArea.setBounds(10, 310, 770, 175);
            immunePanel2.add(antibioticsArea);

            // Flips to first panel about Immune System.
            returnPageBox = new JComboBox();
			returnPageBox.addActionListener(new ReturnBoxListener());
			returnPageBox.addItem("Switch Pages!");
            returnPageBox.addItem("Page One");
            returnPageBox.setFont(font);
            returnPageBox.setBounds(299, 505, 200, 50);
            immunePanel2.add(returnPageBox);

            // Closes the game when clicked.
            JButton quitButton = new JButton("Exit!");
            quitButton.setFont(font);
            quitButton.addActionListener(new ButtonQuitListener());
            quitButton.setBounds(305, 420, 188, 60);
            menuPanel.add(quitButton);
        }
    }

    // Creates Checkerboard background.
    class CheckerBoard extends JPanel
    {
        int ii, jj;         // Used in 'for loop' to draw checkerboard.
        int x, y;           // Coordinates to draw checkerboard.
        int colorCounter;   // Determines which color to use for checkerboard.
        Color color, myDarkColor;   // Colors used to draw checkerboard.

        // Animates the checkerboard design (alternates colors).
        Animate animate;
        Timer checkerboardTimer;

        private class Animate implements ActionListener  // Animate screen.
        {
            public void actionPerformed(ActionEvent e)
            {
                // Switch colors depending on 'colorCounter' value.
                if (colorCounter % 2 == 0)
                {
                    color = Color.blue;
                    myDarkColor = color.darker();
                }
                else
                {
                    myDarkColor = Color.blue;
                    color = myDarkColor.darker();
                }
                // Increment and repaint checkerboard.
                colorCounter++;
				repaint();
            }
        }

        public CheckerBoard()
        {
            colorCounter = 0;
            // Initialize and start animation of checkerboard.
            animate = new Animate();
            checkerboardTimer = new Timer(500, animate);
            checkerboardTimer.start();
        }

        public void paintComponent(Graphics g)
        {
        	// Draw checkerboard pattern.
            super.paintComponent(g);
            for (ii = 0; ii < 8; ii++)
            {
                for (jj = 0; jj < 8; jj++)
                {
                    x = ii * 100;   // Convert to pixel values.
                    y = jj * 73;    // Convert to pixel values.

                    // Set the colors of the checkerboard.
                    if ((ii % 2) == (jj % 2))
                    {
                        g.setColor(myDarkColor);
                    }
                    else
                    {
                        g.setColor(color);
                    }
                    g.fillRect(x, y, 100, 73);  // Draw rectangles of the board.
                }
            }
            ii = jj = 0;    // Reset values so 'for loop' can rerun.
        }
    }

    // Game Panel. Where game will be played on/take place.
    class WBCGamePanel extends JPanel implements KeyListener
    {
        SpriteAnimation sa; // Animates the white blood cells, germs, and background.
        CreateGerms cg; // Creates and places germs on board.
        MoveGerms mg;   // Moves germs down on board.

        // Explosions when bullet hits enemy.
        ExplosionAnimation ea;
        int explosionPixelX, explosionPixelY;

        int counter;    // Determines which subimage to draw
		// Where to draw background.
        int xStart, yStart, backgroundHeight, backgroundWidth;

        // Used to store, place, and position the cell on the game board.
        ArrayList<Point> cells;
        Point cellPoint;
        int cellPixelX, cellPixelY;

        // Used to store , place, and position germs on game board.
	    ArrayList<GameObject> germs;
        Point germPoint;
        int germPixelX, germPixelY;

        // Used to store, place, and position bullets on game board.
        ArrayList<Point> bullets;
        int bulletPixelX, bulletPixelY;
        int whenToDrawBullets;  // Bullets only drawn when spacebar is pressed.

        // Height and width of each bullet.
        int bulletWidth = 45;
        int bulletHeight = 60;

        int timeToDrawCounter;  // Determines how long germs should spawn.
        int explosionCounter;   // Picks which explosion image to draw.
        int pauseCounter;       // Determines if timers should be started or stopped when 'p' is pressed.

        // JButtoms that allow user to navigate paused game.
        JButton howToPlayButton, immunePauseButton, quitPauseButton;
        JLabel pauseMessage;    // Displays message when game is paused.

        int randomNumber;  // Gets a random BufferedImage for enemies
        int score;         // User's score that is increased when enemy is defeated.

        // Animates white blood cells, germs, and backgrounds [sprites in general].
        class SpriteAnimation implements ActionListener
        {
            public void actionPerformed (ActionEvent e)
            {
                if (counter > 4) // Reset counter to run animations.
                {
                    counter = 1;
                }

        		// Get images for white blood cell (user).
        		drawing = objectCells.get(counter-1);

                // Get the next image in each ArrayList for animation effect.
        		for (GameObject newGerm : germs)
        		{
        		    newGerm.getNextImage();
        		}

        		// Get images for bullets.
                 bulletDrawing = objectBullets.get(counter-1);

        		// Get coordinates for background image.
                backgroundHeight = backgroundWidth = (counter * 100) + 800;

                counter++; // Increment to get next image.
                repaint(); // Repaint game with newly acquired images.
            }
        }

        // Creates and places enemies on board.
        class CreateGerms implements ActionListener
        {
            public void actionPerformed (ActionEvent ae)
            {
                generateEnemies(); // Create germs.

                // Stop generating germs after some time.
                if (timeToDrawCounter == 299)    // Make 300 germs.
                {
                    createGerms.stop(); // Stop after 300th germ.
                }
                timeToDrawCounter++; // Increment to draw more germs.
            }
        }

        // Moves the germs down towards user.
        class MoveGerms implements ActionListener
        {
            public void actionPerformed(ActionEvent ae) // Move germs down board.
            {
                for (int ii = 0; ii < germs.size(); ii++)
                {
                    Point germPoint = germs.get(ii).p;
                    germPoint.y += 1;

                    // If an enemy leaves the board.
                    if (germPoint.y > 10)
                    {
                    	mistakesMade++;  // Increment because user has made an error.
                        System.out.println("An enemy escaped!"); // Notify user.
                        germs.remove(ii);   // Remove germ from board.

                        // Stop gameplay timers.
                        generalAnimations.stop();
                        explosionTimer.stop();
                        createGerms.stop();
                        moveGerms.stop();

                        // Show panel that asks user a question when game isnt' over.
                        if (mistakesMade < 10)
                        {
                            switchPanel.show(titlePanel, "Ask Questions");
                        }
                    }
                }
            }
        }

        // Animates the explosion when enemy is hit by bullet.
        class ExplosionAnimation implements ActionListener
        {
            public void actionPerformed (ActionEvent ae)
            {
                // Get appropriate BufferedImage for the explosion.
                explosionDrawing = objectExplosion.get(explosionCounter-1);
                explosionCounter++;  // Increment to get next BufferedImage.
                if (explosionCounter > 13)  // Only draw one explosion when called.
                {
                	// Stop after one whole explosion is animated.
                    explosionCounter = 1;
                    explosionTimer.stop();
                }
                repaint();  // Repaint for fluid animation.
            }
        }

        public WBCGamePanel()
        {
            frame.addKeyListener(this);

            // Components that appear when game is paused.
            howToPlayButton = new JButton("How to Play!");
            immunePauseButton = new JButton("Immune System!");
            quitPauseButton = new JButton("Quit!");
            pauseMessage = new JLabel("GAME PAUSED - 'P' TO RESUME");

            // Creates random location of cell and adds to arraylist.
            cells = new ArrayList<Point>();
            cellPoint = randomPointCell();
            cells.add(cellPoint);

	        germs = new ArrayList<GameObject>();  // Contains germ locations and drawings.
            bullets = new ArrayList<Point>();   // Contains bullets.
            whenToDrawBullets = 0;    // Draw bullet after values set.

	        // Used to draw background.
            backgroundWidth = backgroundHeight = 800;

            // Animate the cell, germs, and background.
            sa = new SpriteAnimation();
            generalAnimations = new Timer(200, sa);

            // Create the germs and place them on the game board.
            cg = new CreateGerms();
            createGerms = new Timer(500, cg);

            // Move the germs around on the board.
            mg = new MoveGerms();
            moveGerms = new Timer(500, mg);

            // Animate the explosion.
            ea = new ExplosionAnimation();
            explosionTimer = new Timer(60, ea);

            timeToDrawCounter = 0; // Maintains limit of objects to be drawn.
            pauseCounter = 0;      // Whether to stop/start timers.
            explosionCounter = counter = 1;    // Counter for animations.
            score = 0; // User starts with zero points.

            generalAnimations.start(); // Start timer to animate images.
        }

        public void generateEnemies()  // Create enemies to place on board.
        {
            // Generate random locations for enemies.
            germPoint = randomPointGerm();

            // Assign each enemy with random BufferedImage [different colored].
    	    int randomNumber = (int)(Math.random() * 3);
    	    BufferedImage germDrawing = germAnimations.get(randomNumber).get(0);

    	    // ArrayList used to temporarily hold locations of enemy.
    	    ArrayList<Point> germPoints = new ArrayList<Point>();

    	    // Store locations of enemies into germPoints ArrayList.
    	    for (int ii = 0; ii < germs.size(); ii++)
    	    {
    		    germPoints.add(germs.get(ii).p);  // Add/Get location of each enemy.
    	    }
    	    // Check existing ArrayList for duplicate locations.
    	    while (germPoints.contains(germPoint))
    	    {
    		    germPoint = randomPointGerm();  // Get a new location to get unique germs.
    	    }
    	    // Add location and image of enemy to ArrayList to "create" it.
    	    germs.add(new GameObject(germDrawing, germPoint,
				     			    germAnimations.get(randomNumber)));
        }

        public void moveBullets() // Moves the bullet upwards after being fired.
        {
           // Create new ArrayList to hold updated points.
           ArrayList<Point> newBullets = new ArrayList<Point>();
           for (Point bullet : bullets)  // Get every bullet and move it.
           {
	          // Move each bullet up one space.
              Point newBullet = new Point(bullet.x, bullet.y - 1);
              newBullets.add(newBullet);    // Add updated bullet to ArrayList.

              // ArrayList that will hold locations of enemies temporarily.
              ArrayList<Point> germPoints = new ArrayList<Point>();
    	      for (int ii = 0; ii < germs.size(); ii++)
    	      {
    	          // Get only the location [Point] of the germ from the GameObject ArrayList.
    		      germPoints.add(germs.get(ii).p);
    	      }

              if (germPoints.contains(newBullet))  // If bullet hits enemy.
              {
                  newBullets.remove(newBullet);  // Remove bullet from game board.
				  int index = germPoints.indexOf(newBullet);  // Get the hit enemy.
                  germs.remove(index);  // Remove enemy from game board.

                  score += 5; // Add five points to user's score total when enemy is hit.

                  // Store user's score inside of a text file named 'userscore.txt'.
                  scoreOutputFile = new File("userscore.txt");

                  try // Try to create and write to seperate .txt file.
    		      {
    			      scoreOutput = new PrintWriter(scoreOutputFile);
    		      }
		          catch (FileNotFoundException fnfe)
			      // Print out error if unable to read file.
    		      {
    			      System.err.println("ERROR: Cannot open text file");
    			      System.exit(1);
    		      }
                  // Print out user's score on a seperate .txt file.
		          scoreOutput.println("Your total score was " + score
					                   + " points. Great job!");
		          scoreOutput.close();

                  System.out.println("Your score is: " + score); // Print out user's current score.

                  // Set location of explosion to where bullet hit enemy.
		  		  explosionPixelX = getPixelX(newBullet.x) - 3;
				  // Get x-Pixel location of explosion.
				  explosionPixelY = getPixelY(newBullet.y);
		   		  // Get y-Pixel location of explosion.
                  explosionTimer.start();  // Start animation of explosions.
              }
           }
           bullets = newBullets;   // Set old ArrayList to new one with updated bullets.
        }

        public Point randomPointCell()  // Generate a Random point for cell.
        {
            return new Point((int)(Math.random() * 10), 9);
        }

        public Point randomPointGerm()  // Generate a random Point for germ.
        {
            return new Point((int)(Math.random() * 10), 0);
        }

        public int getPixelX(int generatedNumber)   // Generate x-Pixel locations from random point.
        {
            return (generatedNumber * 80);
        }

        public int getPixelY(int generatedNumber)   // Generate y-Pixel locations from random point.
        {
            return (generatedNumber * 58);
        }

        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);

            // Draw background.
            g.drawImage(backgroundSprite, 0, 0, backgroundWidth, backgroundHeight, this);

            // Draw explosions.
            g.drawImage(explosionDrawing, explosionPixelX, explosionPixelY, 70, 40, this);

            // Get valid pixel locations for white blood cell and draw.
            for (Point whiteBloodCellPoint : cells)
            {
            	// Get x-Pixel location of white blood cell [user].
                cellPixelX = getPixelX(whiteBloodCellPoint.x);
                // Get y-Pixel location of white blood cell [user].
                cellPixelY = getPixelY(whiteBloodCellPoint.y);

				// Store every enemy location in a new ArrayList of Points.
        		ArrayList<Point> germPoints = new ArrayList<Point>();
        		for (int ii = 0; ii < germs.size(); ii++)
        		{
			        // Get/Add the location to the ArrayList.
        			germPoints.add(germs.get(ii).p);
        		}

        		// If enemy lands on user.
        		if (germPoints.contains(whiteBloodCellPoint))
        		{
        			mistakesMade++;  // Increment because user has made an error.
        			System.out.println("Hit by germ!"); // Notify user how they lost.

                    // Stop all timers to stop gameplay.
        			generalAnimations.stop();
        			explosionTimer.stop();
        			createGerms.stop();
        			moveGerms.stop();

                    // Show panel that askes user questions when game isn't over.
                    if (mistakesMade < 10)
                    {
        			    switchPanel.show(titlePanel, "Ask Questions");
                    }
        		}
        		// Draw white blood cell [user].
        		g.drawImage(drawing, cellPixelX, cellPixelY, 60, 70, this);
        	}

            // Get valid pixels locations for germs and draw.
    	    for (GameObject germ : germs)
    		{
    			// If at least one enemy passes white blood cell [user] and leaves board.
                if (returnPlay != false)
                {
                	// Move all existing enemies five places back
                    germ.p.y -= 5;
                }
                // Draw the germ.
    		    g.drawImage(germ.drawing,
    		    			getPixelX(germ.p.x), getPixelY(germ.p.y) - 10,
							50, 60, this);
    		}
            returnPlay = false;  // Return to normal method of drawing enemies.

    	    if (whenToDrawBullets > 0)  // Draw bullets after values are set.
    		{
    		    for (Point bullet : bullets)  // Draw every bullet that is created.
    		    {
		    		g.drawImage(bulletDrawing,
								getPixelX(bullet.x), getPixelY(bullet.y) - 50,
								bulletWidth, bulletHeight, this);
    		    }

    		    if (pauseBoolean != true)   // If game is in play.
    			{
    			    moveBullets(); // Move the bullets across the board.
    			}
    		}

    	    if (pauseBoolean != false)  // If game is paused.
    		{
    		    // Black rectangle shown when game is paused for visibility of text.
    		    g.setColor(Color.black);
    		    g.fillRect(70, 25, 650, 50);
    		}

    		if (mistakesMade == 10)  // If user has made 10 errors, end game.
    		{
    			// Stop all timers.
    			createGerms.stop();
    			moveGerms.stop();
    			explosionTimer.stop();
    			generalAnimations.stop();

    			// Show panel that indicates game is over.
    			switchPanel.show(titlePanel, "Game Over");
			}
    	}

        public void keyPressed(KeyEvent e)
        {
            char c = e.getKeyChar();
            if (c == 'p') // Pause game
            {
                pauseCounter++;   // Increment to know if 'pause' or 'unpause.'
                if (pauseCounter % 2 == 0) // Start game again
                {
                    pauseBoolean = false;  // Game is not paused.

                    // Restart timers.
                    generalAnimations.start();
                    createGerms.start();
                    moveGerms.start();

                    // Remove JComponents if game is not paused.
                    remove(howToPlayButton);
                    remove(immunePauseButton);
                    remove(quitPauseButton);
                    remove(pauseMessage);
                }

                else // Pause game.
                {
                    pauseBoolean = true;  // Game is paused.

                    // Stop timers.
                    generalAnimations.stop();
                    explosionTimer.stop();
                    createGerms.stop();
                    moveGerms.stop();

                    // JButton to return to titlescreen.
                    howToPlayButton.setFont(font);
                    howToPlayButton.addActionListener(new ButtonInfoListener());
                    howToPlayButton.setBounds(305, 180, 188, 60);
                    add(howToPlayButton);

                    // JButton to show immune system info.
                    immunePauseButton.setFont(new Font("Arial", Font.BOLD, 18));
                    immunePauseButton.addActionListener(new ButtonImmuneListener());
                    immunePauseButton.setBounds(305, 260, 188, 60);
                    add(immunePauseButton);

                    // JButton that returns to main menu.
                    quitPauseButton.setFont(font);
                    quitPauseButton.addActionListener(new ButtonMainListener());
                    quitPauseButton.setBounds(305, 340, 188, 60);
                    add(quitPauseButton);

                    // Message that instructs user how to resume game.
                    pauseMessage.setFont(new Font("Arial", Font.BOLD, 40));
                    pauseMessage.setForeground(Color.white);
                    pauseMessage.setBounds(80, 25, 680, 50);
                    add(pauseMessage);
                }
            }

            if (pauseBoolean != true)   // Only move when game is not paused.
            {
                if (c == 'a') // Move left.
                {
                    cellPoint.x -= 1;
                    if (cellPoint.x < 0) // Flip to other side if moving off board.
                    {
                        cellPoint.x = 9;
                    }
                }

                if (c == 'd') // Move right.
                {
                    cellPoint.x += 1;
                    if (cellPoint.x > 9) // Flip to other side if moving off board.
                    {
                        cellPoint.x = 0;
                    }
                }

                if (e.getKeyCode() == KeyEvent.VK_SPACE)    // Spacebar pressed.
                {
                    // Create bullet and add to ArrayList.
                    Point generatedBullet = new Point(cellPoint.x, cellPoint.y);
                    bullets.add(generatedBullet);
                    whenToDrawBullets++;
                }
            }
            repaint();
        }
        public void keyReleased(KeyEvent e){}
        public void keyTyped(KeyEvent e){}
    }

    // Asks the user a question about the immune system.
    class QuestionPanel extends JPanel
    {
        public QuestionPanel()
        {
            setBackground(Color.black);

            // Welcome user to Questioning screen/panels.
            JTextArea welcomeField = new JTextArea("QUESTION & ANSWER!");
            welcomeField.setForeground(Color.WHITE);
            welcomeField.setOpaque(false);
            welcomeField.setFont(new Font("Utopia", Font.BOLD, 20));
            welcomeField.setEditable(false);
            welcomeField.setBounds(275, 10, 250, 35);
            add(welcomeField);

            // Quit the game (exit program).
            JButton quitQuestionButton = new JButton("Quit!");
            quitQuestionButton.setFont(new Font("Arial", Font.BOLD, 20));
            quitQuestionButton.addActionListener(new ButtonQuitListener());
            quitQuestionButton.setBounds(10, 10, 215, 40);

            JButton quitQuestionButton2 = new JButton("Quit!");
            quitQuestionButton2.addActionListener(new ButtonQuitListener());
            quitQuestionButton2.setFont(new Font("Arial", Font.BOLD, 20));
            quitQuestionButton2.setBounds(550, 10, 225, 40);

            // Generate a question for the user to answer.
            JButton generateQuestionButton = new JButton("Get Question!");
            generateQuestionButton.addActionListener(new GenerateQuestionListener());
            generateQuestionButton.setFont(new Font("Arial", Font.BOLD, 20));
            generateQuestionButton.setBounds(0, 520, 800, 50);

            // Add the JButtons to the panel.
            add(quitQuestionButton);
            add(quitQuestionButton2);
            add(generateQuestionButton);

            // Tells user about the questioning system.
            JTextArea triviaArea = new JTextArea(triviaString);
            triviaArea.setForeground(Color.white);
            triviaArea.setFont(new Font("Arial", Font.BOLD, 18));
            triviaArea.setOpaque(false);
            triviaArea.setEditable(false);
            triviaArea.setLineWrap(true);
            triviaArea.setWrapStyleWord(true);
            triviaArea.setBounds(30, 260, 780, 125);
            add(triviaArea);
        }
    }

    // Pick a random question to ask user.
    class GenerateQuestionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
        	// Generate random number to pick random question.
	        questionPicker = (int)(Math.random() * 18);

            switch (questionPicker) // Pick question according to generated number.
            {
                case 0: switchPanel.show(titlePanel, "First Question"); break;
                case 1: switchPanel.show(titlePanel, "Second Question"); break;
                case 2: switchPanel.show(titlePanel, "Third Question"); break;
                case 3: switchPanel.show(titlePanel, "Fourth Question"); break;
                case 4: switchPanel.show(titlePanel, "Fifth Question"); break;
                case 5: switchPanel.show(titlePanel, "Sixth Question"); break;
	            case 6: switchPanel.show(titlePanel, "Seventh Question"); break;
                case 7: switchPanel.show(titlePanel, "Eighth Question"); break;
                case 8: switchPanel.show(titlePanel, "Ninth Question"); break;
                case 9: switchPanel.show(titlePanel, "Tenth Question"); break;
                case 10: switchPanel.show(titlePanel, "Eleventh Question"); break;
                case 11: switchPanel.show(titlePanel, "Twelvth Question"); break;
                case 12: switchPanel.show(titlePanel, "Thirteenth Question"); break;
                case 13: switchPanel.show(titlePanel, "Fourteenth Question"); break;
                case 14: switchPanel.show(titlePanel, "Fifteenth Question"); break;
                case 15: switchPanel.show(titlePanel, "Sixteenth Question"); break;
                case 16: switchPanel.show(titlePanel, "Seventeenth Question"); break;
                case 17: switchPanel.show(titlePanel, "Eighteenth Question"); break;
            }
        }
    }

    // Sets up Question panels so user can answer them.
    class Question extends JPanel
    {
    	int questionNumber;    // Number of question that will be asked.
    	String correctAnswer;  // Contains the correct answer for each question.
    	String incorrectAnswer, incorrectAnswer1;  // Contains the incorrect answers for each question.

		// Take the variables above as arguments for each question.
    	public Question(int _questionNumber,
    			        String _correctAnswer,
    			        String _incorrectAnswer, String _incorrectAnswer1)
    	{
    	    setBackground(Color.black);
    	    questionNumber = _questionNumber;
    	    correctAnswer = _correctAnswer;
    	    incorrectAnswer = _incorrectAnswer;
    	    incorrectAnswer1 = _incorrectAnswer1;
    	}

    	public void paintComponent(Graphics g)
    	{
    	    super.paintComponent(g);

    	    // Display the question.
    	    JTextArea questionArea = new JTextArea(questionHolder.get(questionNumber));
            questionArea.setForeground(Color.white);
            questionArea.setBackground(Color.black);
            questionArea.setFont(font);
    	    questionArea.setEditable(false);
            questionArea.setLineWrap(true);
    	    questionArea.setWrapStyleWord(true);
    	    questionArea.setBounds(100, 80, 600, 50);
    	    add(questionArea);

			// Hold both correct and incorrect options in an ArrayList.
    	    ArrayList<String> textOptions = new ArrayList<String>();
    	    textOptions.add(correctAnswer);
    	    textOptions.add(incorrectAnswer);
    	    textOptions.add(incorrectAnswer1);

            // ButtonGroup so user can only pick one JRadioButton.
    	    ButtonGroup questionGroup = new ButtonGroup();

    	    // Create and place JRadioButtons on panel as possible answers.
    	    for (int ii = 0; ii < 3; ii++)
    	    {
        		// X/Y-pixel locations, width, and height of JRadioButtons.
        		int y = ii * 150 + 150;
        		int x = 60;
        		int width = 700;
        		int height = 150;

        		JRadioButton questionButton = new JRadioButton(); // Possible answers stored in these.

        		if (ii == 0) // Assign ActonListener for when correct answer is chosen.
        		{
        		    questionButton.addActionListener(new CorrectAnswerListener(questionNumber));
        		}

        		else // Assign ActionListener for when incorrect answer is chosen.
        		{
        		    questionButton.addActionListener(new IncorrectAnswerListener(questionNumber));
        		}

        		// Set properties of JRadioButtons.
        		questionButton.setOpaque(false);
        		questionButton.setForeground(Color.cyan);
        		questionButton.setFont(mediumFont);
        		questionButton.setBounds(x, y, width, height);
        		questionButton.setText(textOptions.get(ii));
        		this.add(questionButton);
        		questionGroup.add(questionButton);
    	    }
    	}
    }

    // Panel shown when a question is answered correctly.
    class CorrectAnswer extends JPanel
    {
        Color green, darkGreen;  // Colors used for painting.

        public CorrectAnswer()
        {
            green = Color.green;
            darkGreen = green.darker();
            setBackground(darkGreen);
        }

	    public void paintComponent(Graphics g)
        {
            super.paintComponent(g);

            // Tell user they're correct.
            JTextArea correctField = new JTextArea("YOU ARE RIGHT!");
            correctField.setForeground(Color.WHITE);
            correctField.setOpaque(false);
            correctField.setFont(new Font("Utopia", Font.BOLD, 35));
            correctField.setEditable(false);
            correctField.setBounds(230, 10, 500, 35);
            add(correctField);

            // Display the correct answer to the question.
            JTextArea correctAnswerArea = new JTextArea(answerHolder.get(questionPicker));
            correctAnswerArea.setForeground(Color.WHITE);
            correctAnswerArea.setBackground(darkGreen);
            correctAnswerArea.setFont(new Font("Arial", Font.BOLD, 20));
            correctAnswerArea.setEditable(false);
            correctAnswerArea.setLineWrap(true);
            correctAnswerArea.setWrapStyleWord(true);
            correctAnswerArea.setBounds(100, 240, 620, 150);
            add(correctAnswerArea);

			// Allow user to return to game play.
            JButton correctReturn = new JButton("Return to Game!");
            correctReturn.setFont(new Font("Arial", Font.BOLD, 20));
            correctReturn.addActionListener(new ButtonReturnPlayListener());
            correctReturn.setBounds(0, 520, 800, 50);
            add(correctReturn);
	    }
    }

    // Panel shown when a question is answered incorrectly.
    class IncorrectAnswer extends JPanel
    {
        Color red, darkRed;  // Colors used for painting.

        public IncorrectAnswer()
        {
            red = Color.red;
            darkRed = red.darker();
            setBackground(darkRed);
        }

        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);

            // Tell user that they're incorrect.
            JTextArea incorrectField = new JTextArea("YOU ARE WRONG!");
            incorrectField.setForeground(Color.WHITE);
            incorrectField.setOpaque(false);
            incorrectField.setFont(new Font("Utopia", Font.BOLD, 35));
            incorrectField.setEditable(false);
            incorrectField.setBounds(230, 10, 500, 35);
            add(incorrectField);

            // Display the correct answer to the question.
            JTextArea incorrectAnswerArea = new JTextArea(answerHolder.get(questionPicker));
            incorrectAnswerArea.setForeground(Color.WHITE);
            incorrectAnswerArea.setBackground(darkRed);
            incorrectAnswerArea.setFont(new Font("Arial", Font.BOLD, 20));
            incorrectAnswerArea.setEditable(false);
            incorrectAnswerArea.setLineWrap(true);
            incorrectAnswerArea.setWrapStyleWord(true);
            incorrectAnswerArea.setBounds(100, 240, 620, 150);
            add(incorrectAnswerArea);

            // Allow user to return to game play.
            JButton incorrectReturn = new JButton("Return to Game!");
            incorrectReturn.setFont(new Font("Arial", Font.BOLD, 20));
            incorrectReturn.addActionListener(new ButtonReturnPlayListener());
            incorrectReturn.setBounds(0, 520, 800, 50);
            add(incorrectReturn);
        }
    }

    // Displays the screen that indicates the game is over.
    class GameOver extends JPanel
    {
    	public GameOver()
		{
			setBackground(Color.black);

            // Tells user the game is over.
            JTextField gameOverField = new JTextField("GAME OVER! YOU LOSE!");
            gameOverField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
            gameOverField.setForeground(Color.RED);
            gameOverField.setBackground(new Color(0, 0, 0, 0));
            gameOverField.setFont(new Font("Arial", Font.BOLD, 35));
            gameOverField.setEditable(false);
            gameOverField.setBounds(185, 20, 500, 120);
            add(gameOverField);

            // Tell user last message.
            JTextArea gameOverArea = new JTextArea(gameOverString);
            gameOverArea.setForeground(Color.RED);
            gameOverArea.setFont(new Font("Arial", Font.BOLD, 20));
            gameOverArea.setBackground(Color.BLACK);
            gameOverArea.setEditable(false);
            gameOverArea.setLineWrap(true);
            gameOverArea.setWrapStyleWord(true);
            gameOverArea.setBounds(100, 240, 620, 150);
            add(gameOverArea);

			// Quit game when clicked to end game.
            JButton gameOverButton = new JButton("Quit - End Game!");
            gameOverButton.setFont(new Font("Arial", Font.BOLD, 20));
            gameOverButton.addActionListener(new ButtonQuitListener());
            gameOverButton.setBounds(0, 530, 800, 50);
            add(gameOverButton);
		}
	}

    // Displays screen that indicates that user answer was correct.
    class CorrectAnswerListener implements ActionListener
    {
    	int questionNumber;  // Number of question that was selected.

    	public CorrectAnswerListener(int _questionNumber)
    	{
    	    questionNumber = _questionNumber;
    	}
        public void actionPerformed(ActionEvent ae)
        {
        	// Flip to the chosen panel.
            switchPanel.show(titlePanel, "Correct Answer " + (questionNumber + 1));
        }
    }

    // Displays screen which indicates that user answer was incorrect.
    class IncorrectAnswerListener implements ActionListener
    {
        int questionNumber;  // Number of the question that was selected.

        // Take the number of the question as an argument.
        public IncorrectAnswerListener(int _questionNumber)
        {
            questionNumber = _questionNumber;
        }

        public void actionPerformed(ActionEvent ae)
        {
        	// Flip to the chosen panel.
            switchPanel.show(titlePanel, "Incorrect Answer " + (questionNumber + 1));
        }
    }

    // Return to main menu.
    class ButtonMainListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            switchPanel.show(titlePanel, "Main Menu");
        }
    }

    // Go to game panel screen to play.
    class ButtonPlayListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            switchPanel.show(titlePanel, "WBC Mini-Game"); // Go to game panel.

            if (pauseBoolean != true)   // Start playing the game.
            {
            	// Restart timers to restart game play.
                createGerms.start();
                moveGerms.start();
            }
        }
    }

    // Go to game panel screen to play game from Incorrect/Correct panels.
    class ButtonReturnPlayListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            switchPanel.show(titlePanel, "WBC Mini-Game"); // Go to game panel.

            // Restart timers to restart game.
            returnPlay = true;
            explosionTimer.start();
            createGerms.start();
            moveGerms.start();
            generalAnimations.start();
        }
    }

    // Get the next panel about Instructions.
    class ButtonInfoListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            switchPanel.show(titlePanel, "Instructions");
        }
    }

    // Flips to page as instructed by JRadioButton.
    class RadioListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
        	if (infoPageButton.isSelected()) // Go to panel with Instructions.
        	{
        		switchPanel.show(titlePanel, "Instructions");
			}

			if (enemyPageButton.isSelected()) // Go to panel with info about enemies.
			{
				switchPanel.show(titlePanel, "Info about Germs");
			}
		}
	}

	// Go to second panel about the Immune System.
	class ChangeBoxListener implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			if (changePageBox.getSelectedItem().equals("Page Two")) // If this option is selected.
			{
				switchPanel.show(titlePanel, "Immune System Page 2"); // Flip to second panel.
				changePageBox.setSelectedItem("Switch Pages!"); // Add option so user can flip back.
			}
		}
	}

	// Go to first panel about the Immune System.
	class ReturnBoxListener implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			if (returnPageBox.getSelectedItem().equals("Page One")) // If this option is selected.
			{
				switchPanel.show(titlePanel, "Immune System");  // Flip to first panel.
				returnPageBox.setSelectedItem("Switch Pages!"); // Add option so user can flip back.
			}
		}
	}

    // Show panel that will contain info about the Immune System.
    class ButtonImmuneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            switchPanel.show(titlePanel, "Immune System");
        }
    }

    // Close the program when JButton is clicked.
    class ButtonQuitListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            System.exit(0); // Close program.
        }
    }
}

// Stoes BufferedImages of the enemies and is used for animation.
class GameObject
{
    public BufferedImage drawing;   // BufferedImage of enemy.
    ArrayList<BufferedImage> animation;   // An ArrayList of BufferedImages of enemy for animations.
    public Point p;     // X-Y Point location of each enemy on board.

    // Take these variables as arguments.
    public GameObject (BufferedImage _img, Point _p, ArrayList<BufferedImage> _animation)
    {
        drawing = _img;
	    p = _p;
	    animation = _animation;
    }

     // Return the next picture in the ArrayList of BufferedImages for animations.
    public BufferedImage getNextImage()
    {
    	int indexOfImage = animation.indexOf(drawing); // Get position of current image of enemy.
    	int nextIndex = (indexOfImage + 1) % animation.size(); // Get position of next image of enemy.
    	drawing = animation.get(nextIndex);    // Get the BufferedImage at the next position.
    	return drawing;    // Return that BufferedImage for animations.
    }
}